/*
    Archivo: getmemory.c

    Descripción: Calcula la memoria disponible en el computador, es necesario que
                el computador cuente con un sistema linux, ya que el programa ingresa
                a /proc/meminfo y obtiene la memoria libre "MemFree"

    Compilación: gcc getmemory.c -o getmemory -lm

    Ejecución: ./getmemory

    Autores: Felipe Parra, Fernando García

*/

#include "stdlib.h"
#include "stdio.h"
#include "string.h"
#include "math.h"

int parseLine(char* line){
    // This assumes that a digit will be found and the line ends in " Kb".
    int i = strlen(line);
    const char* p = line;
    while (*p <'0' || *p > '9') p++;
    line[i-3] = '\0';
    i = atoi(p);
    return i;
}

int getValue(){ //Note: this value is in KB!
    FILE* file = fopen("/proc/meminfo", "r");
    int result = -1;
    char line[128];

    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "VmSize:", 7) == 0){
            result = parseLine(line);
            break;
        }
    }
    fclose(file);
    return result;
}

int main(int argc, char *argv[])
{
    FILE* file = fopen("/proc/meminfo", "r");
    int result = -1;
    char line[128];
    while (fgets(line, 128, file) != NULL){
        if (strncmp(line, "MemFree:", 8) == 0){
            result = parseLine(line);
            break;
        }
    }

    int n = atoi(argv[1]);
    double size = sqrt(result / 3);

    printf("\nMemFree: %d\n", result);
    printf("Tamaño máximo para el tamaño de las matrices: %f\n", size);
    printf("\nSe pueden crear %d matrices de tamaño ", n);
    printf("%f\n", size / (double)n);

    printf("\n%d\n", 12/8 + 12%8);

    return 0;
}