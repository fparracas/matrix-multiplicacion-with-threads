#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

int cantidadRondas = 0;

void *matrixMultiplication(void *vargp)
{
	printf("In funtion \nthread id = %d\n", pthread_self()); 
	char **argumentos;
	argumentos = (char **)vargp;

	int i, j, k, l;
	int rowsAndColumns = atoi(argumentos[2]);
    unsigned char **emptyMatrixA;
    unsigned char **emptyMatrixB;
    unsigned char **emptyMatrixC;

	// M A T R I X A
	    emptyMatrixA = calloc(rowsAndColumns + 2,sizeof(unsigned int *));
	    for (i = 0; i < rowsAndColumns + 1; i = i + 1)
	    {
	       emptyMatrixA[i] = calloc(rowsAndColumns + 1,sizeof(unsigned int));
	    }
	// M A T R I X B
	    emptyMatrixB = calloc(rowsAndColumns + 2,sizeof(unsigned int *));
	    for (i = 0; i < rowsAndColumns + 1; i = i + 1)
	    {
	       emptyMatrixB[i] = calloc(rowsAndColumns + 1,sizeof(unsigned int));
	    }
	// M A T R I X C
	    emptyMatrixC = calloc(rowsAndColumns + 2,sizeof(unsigned int *));
	    for (i = 0; i < rowsAndColumns + 1; i = i + 1)
	    {
	       emptyMatrixC[i] = calloc(rowsAndColumns + 1,sizeof(unsigned int));
	    }


    // M A T R I X, M U L T I P L I C A T I O N
	    int cantVueltas = atoi(argumentos[1])/atoi(argumentos[3]);
	   	cantVueltas = cantVueltas + atoi(argumentos[1]) % atoi(argumentos[3]);
	    for(l = 0; l < cantVueltas ; ++l)
	    {
			for (i = 0; i < rowsAndColumns; ++i)
			{
				for(j = 0; j < rowsAndColumns; ++j)
				{
					for(k = 0; k < rowsAndColumns; ++k)
					{
						emptyMatrixC[i][j] += emptyMatrixA[i][k] * emptyMatrixB[k][j];
					}
				}
			}
			cantidadRondas = cantidadRondas + 1;
			printf("\tCantidad de Rondas: %d\n", cantidadRondas);
		}
	//printf ("\nUn proceso utilizó: MatrixMultiplication!\n");
	return NULL;
}

void *farmMaster(void *vargp)
{
	printf("\nEstoy en el proceso principal: farmMaster\n");
	char **argumentos;
	argumentos = (char **)vargp;
	printf("\nArgumentos ingresados: %s,", argumentos[1]);
	printf(" %s\n", argumentos[2]);

	//  Declaración de las hebras y variables.
		int        cantThreads = atoi(argumentos[3]), i;
		pthread_t  threads[cantThreads];

	// Creación de hebras, se les envía los argumentos ingresados ya que se utilizan
	// para crear las matrices dinámicas y ver la cantidad de rondas que hará cada hebra
	    for (i = 0; i < cantThreads; ++i)
	    {
	    	//int *p;
	    	pthread_create(&threads[i], NULL, matrixMultiplication, (void*)argumentos);
	    }

	// Esperando or
	    for (i = 0; i < cantThreads; ++i)
	    {
	    	pthread_join(threads[i], NULL);
	    }

	return NULL;
}

//argv[1] = n : veces que se multiplicará la matriz
//argv[2] = m : tamaño de la matriz
//argv[3] = p : cantidad de procesos: 2, 4, 8, 16.

int main(int argc, char **argv)
{
   	/// C R E A C I O N, M A T R I Z  V A C I A
   	//Matriz con 0's que se multiplicará n veces



	pthread_t threadMaster;

	printf("Before ThreadMaster\n");

	// manda una función a una hebra en particular, en este caso "matrixMultiplication"
	// a la hebra "pthreadMaster"
	pthread_create(&threadMaster, NULL, farmMaster, (void *) argv);
    pthread_join(threadMaster, NULL); 

    printf("After ThreadMaster\n"); 

    
	return 0;
}